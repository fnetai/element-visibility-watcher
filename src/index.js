/**
 * @module elementVisibilityWatcher
 * @description Monitors an HTML element and triggers a callback when the element is no longer in view or is removed from the DOM.
 * @param {Object} args - The arguments object.
 * @param {HTMLElement} args.element - The HTML element to monitor.
 * @param {Function} args.callback - The callback function to invoke when the element is no longer in view or is removed.
 * @param {boolean} [args.autoDisconnect=true] - Whether to automatically disconnect the observer before invoking the callback.
 */

export default async ({ element, callback, autoDisconnect = true }) => {
  if (!(element instanceof HTMLElement)) {
    throw new Error("The provided element is not a valid HTML element.");
  }
  if (typeof callback !== "function") {
    throw new Error("The provided callback is not a valid function.");
  }

  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (!entry.isIntersecting || entry.target === null) {
        if (autoDisconnect) {
          observer.disconnect();
        }
        callback();
      }
    });
  }, {
    root: null,
    threshold: 0
  });

  observer.observe(element);

  return {
    disconnect: () => {
      observer.disconnect();
    }
  };
};