# @fnet/element-visibility-watcher

The primary purpose of this project is monitoring the visibility of a specific HTML element on the webpage and running a callback function when this element is no longer visible or has been removed from the document structure (DOM).

To start, the user should provide an object containing at least two properties: the HTML element to be watched and the callback function to run when the element goes out of view or gets removed. This project also supports an optional parameter to define whether the observer should automatically disconnect before running the callback function, with this value being `true` by default.

## Functionality

The core functionality of the @fnet/element-visibility-watcher revolves around the Intersection Observer API, a robust and modern system to monitor an HTML element’s visibility within the viewport. 

The observer initiates after you pass the HTML element to monitor and the callback function. If the monitored element stops intersecting the viewport or gets removed from the DOM, the observer triggers and runs the provided callback function. If the user has enabled autoDisconnect (which is true by default), the observer disconnects before executing the callback.

### Error Checks

This project also runs error checking to ensure appropriate usage. It checks if the supplied element is a valid HTML element and if the given callback is a valid function. If these checks fail, it throws an error notifying the user about the incorrect input.

### Manual Disconnect

Users have the ability to manually disconnect the observer to halt monitoring using the returned ‘disconnect’ function.

In conclusion, the @fnet/element-visibility-watcher provides functionality for developers to easily monitor the visibility of particular elements on the page and take action when they are no longer in view or have been removed entirely from the DOM. This utility can be highly useful for tracking user interactions, enhancing user experience, and many other practical implementations.